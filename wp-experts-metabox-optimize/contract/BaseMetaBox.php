<?php

abstract class BaseMetaBox {

	protected $id;

	protected $title;

	protected $screens;

	protected $context;

	protected $priority;

	public function __construct() {
		$this->screens  = [ 'post' ];
		$this->context  = 'normal';
		$this->priority = 'default';
		add_action( 'save_post', [ $this, 'save' ] );
		add_action( 'add_meta_boxes', [ $this, 'register' ] );
	}

	abstract public function register();

	abstract public function display($post);

	abstract public function save($post_id);

}