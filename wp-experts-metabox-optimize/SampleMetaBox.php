<?php

class SampleMetaBox extends BaseMetaBox {
	public function __construct() {
		parent::__construct();
		$this->title = 'متاباکس نمونه';
		$this->id    = 'sample-meta-box';

	}

	public function register() {
		add_meta_box( $this->id, $this->title, [
			$this,
			'display'
		], $this->screens, $this->context, $this->priority );
	}

	public function display( $post ) {
		//html code goes here.
	}

	public function save( $post_id ) {
		//save meta data goes here.
	}
}