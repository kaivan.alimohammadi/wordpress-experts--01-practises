<?php
function doUp() {
	echo "doing Up";
}

function doDown() {
	echo "doing Down";
}

function doRight() {
	echo "doing Right";
}

function doLeft() {
	echo "doing Left";
}

function doOperation( $operation ) {
	$operationFunction = "";
	switch ( $operation ) {
		case 'doUp':
			$operationFunction = 'doUp';
			break;
		case 'doDown':
			$operationFunction = 'doDown';
			break;
		case 'doRight':
			$operationFunction = 'doRight';
			break;
		case 'doLeft':
			$operationFunction = 'doLeft';
			break;
	}
	if ( ! empty( $operationFunction ) && is_callable( $operationFunction ) ) {
		$operationFunction();
	}
}
