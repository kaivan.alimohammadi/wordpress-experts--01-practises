<?php
include "functions.php";
if ( isset( $_POST['submit'] ) ) {
	$operation = $_POST['operation'];
	doOperation( $operation );
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="" method="post">
    <input type="radio" name="operation" id="doUp" value="doUp">
    <label for="doUp">doUp</label>
    <input type="radio" name="operation" id="doDown" value="doDown">
    <label for="doDown">doDown</label>
    <input type="radio" name="operation" id="doRight" value="doRight">
    <label for="doRight">doRight</label>
    <input type="radio" name="operation" id="doLeft" value="doLeft">
    <label for="doLeft">doLeft</label>
    <button name="submit">Send</button>
</form>

</body>
</html>