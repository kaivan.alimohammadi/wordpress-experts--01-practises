<?php
require_once "config.php";

class DB {
	protected $connection;
	protected $table;
	protected $primaryKey;
	protected $stmt;

	public function __construct() {
		try {
			$this->connection = new PDO( 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=UTF8', DB_USER, DB_PASSWORD, array(
				PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_PERSISTENT => true
			) );

		} catch ( PDOException $exception ) {
			echo $exception->getMessage();
		}
	}

	public function find( $id, $columns = array() ) {
		$columns    = ! empty( $columns ) ? rtrim( implode( ',', $columns ), ',' ) : '*';
		$this->stmt = $this->connection->prepare( "SELECT {$columns} FROM {$this->table} WHERE {$this->primaryKey}=:id" );
		$this->stmt->bindParam( ':id', $id );

		return $this->stmt->execute();

	}

	public function update( $columns = array(), $id = null ) {
		$query = "UPDATE {$this->table} SET ";
		array_walk( $columns, function ( $item, $key ) use ( &$query ) {
			$query .= $key . '=:' . $key;
		} );
		if ( ! is_null( $id ) ) {
			$query .= " WHERE {$this->primaryKey}={$id}";
		}
		$this->stmt = $this->connection->prepare( $query );
		foreach ( $columns as $key => $value ) {
			$this->stmt->bindParam( ":{$key}", $value );
		}

		return $this->stmt->execute();
	}

	public function create( $data = array() ) {
		$fields = rtrim( implode( ',', array_keys( $data ) ), ',' );
		$query  = "INSERT INTO {$this->table} ({$fields}) VALUES(";
		foreach ( array_keys( $data ) as $field ) {
			$query .= ":{$field},";
		}
		$query      = rtrim( $query, ',' );
		$query      .= ")";
		$this->stmt = $this->connection->prepare( $query );
		foreach ( $data as $key => $value ) {
			$this->stmt->bindParam( ":{$key}", $value );
		}

		return $this->stmt->execute();

	}

	public function delete( $id ) {
		$this->stmt = $this->connection->prepare( "DELETE FROM {$this->table} WHERE {$this->primaryKey}=:id LIMIT 1" );
		$this->stmt->bindParam( ':id', $id );

		return $this->stmt->execute();
	}

}