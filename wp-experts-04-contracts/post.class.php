<?php
require_once "db.class.php";

class Post extends DB {

	const DRAFT = 1;
	const PUBLISH = 2;
	const FUTURE = 3;

	public function __construct() {
		parent::__construct();
		$this->table      = 'posts';
		$this->primaryKey = 'id';
	}

	public function get_most_commented_posts( $limit = 10 ) {
		$this->stmt = $this->connection->prepare( "SELECT * FROM {$this->table} ORDER BY comments_count DESC LIMIT :limit" );
		$this->stmt->bindParam( ':limit', $limit );

		return $this->stmt->execute();
	}

	public static function get_post_status() {
		return array(
			self::DRAFT   => 'پیش نویس',
			self::PUBLISH => 'منتشر شده',
			self::FUTURE  => 'زمان بندی شده'
		);
	}

	public static function get_post_status_html( $status ) {
		$statuses = self::get_post_status();

		return '<span>' . $statuses[ $status ] . '</span>';
	}

}