<?php


class Singleton {

	private static $instance;

	public static function getInstance() {
		if ( null === static::$instance ) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * private function to prevent make instance with new keyword.
	 * Singleton constructor.
	 */
	private function __construct() {

	}

	/**
	 * private function to prevent copy current instance.
	 */
	private function __clone() {
	}

	/**
	 *private function to prevent serialize current instance.
	 */
	private function __wakeup() {
	}

}

