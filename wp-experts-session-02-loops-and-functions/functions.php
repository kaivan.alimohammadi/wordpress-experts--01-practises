<?php
include "posts.php";
$posts = get_posts();
function have_posts(){
	global $posts;
	if(empty($posts)){
		return false;
	}
	return array_pop($posts);
}